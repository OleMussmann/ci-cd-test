def sum(x, y):
    """add two integers"""
    if (x<0 or y<0): raise ValueError("Negative number was passed.")
    return x+y


def multiply(x, y):
    return x*y
